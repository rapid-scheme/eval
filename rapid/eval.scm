;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-record-type <environment>
  (%make-environment syntactic-environment dependencies)
  environment?
  (syntactic-environment environment-syntactic-environment)
  (dependencies environment-dependencies))

(define (make-environment . import-set*)
  (receive (exports imports dependencies body)
      (analyze-library #f (list (syntax (import ,import-set* ...))))
    (%make-environment (imports->syntactic-environment imports)
		       dependencies)))

(define (eval-expression expression environment)
  (%eval-expression expression
		    (environment-syntactic-environment environment)
		    (environment-dependencies environment)))

(define (%eval-expression expression environment dependencies)
  (parameterize ((current-syntactic-environment
		  environment))
    (let ((expanded-expression (expand-expression expression))
	  (location (gensym 'location)))
      (receive (store)
	  (evaluate-library dependencies
			    (list (syntax (define-values (,location)
					    ,expanded-expression))))
	(cadar store)))))

(define (eval-program program)
  (let*-values
      (((program import-sets)
	(let loop ((program program) (import-sets '()))
	  (if (null? program)
	      (raise-syntax-fatal-error #f "empty program")
	      (syntax-match (car program)
		((import ,import-set* ...)
		 (loop (cdr program) (append-reverse import-set* import-sets)))
		(,_
		 (if (null? import-sets)
		     (raise-syntax-fatal-error #f "no imports")
		     (values program (reverse import-sets))))))))
       ((exports imports dependencies body)
	(analyze-library #f (list (syntax (import ,import-sets ...))
				  (syntax (begin . ,program)))))
       ((environment definitions)
	(expand-library #f exports imports body))
       ((expression)
	(syntax (case-lambda
		  (()
		   ,@definitions
		   (exit))))))
    (%eval-expression expression environment dependencies)))
