;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid eval-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid runtime-environment)
	  (rapid eval))
  (begin
    (define (run-tests)
    
      (test-begin "Evaluation")

      (with-runtime-environment
	  (make-runtime-environment '() '())
	(lambda ()
	  (define environment (make-environment '(rapid primitive)))

	  (test-equal 42 (eval-expression 42 environment))

	  (test-assert
	      (apply
	       (eval-program '((import (rapid primitive))
			       (define-values (x) 10)
			       x))
	       '()))))
      
      (test-end))))
